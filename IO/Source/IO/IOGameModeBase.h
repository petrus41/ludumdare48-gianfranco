// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "IOGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class IO_API AIOGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
