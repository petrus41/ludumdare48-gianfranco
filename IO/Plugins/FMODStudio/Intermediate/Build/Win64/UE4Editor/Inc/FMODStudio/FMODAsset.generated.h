// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FMODSTUDIO_FMODAsset_generated_h
#error "FMODAsset.generated.h already included, missing '#pragma once' in FMODAsset.h"
#endif
#define FMODSTUDIO_FMODAsset_generated_h

#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_SPARSE_DATA
#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_RPC_WRAPPERS
#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFMODAsset(); \
	friend struct Z_Construct_UClass_UFMODAsset_Statics; \
public: \
	DECLARE_CLASS(UFMODAsset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FMODStudio"), NO_API) \
	DECLARE_SERIALIZER(UFMODAsset)


#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUFMODAsset(); \
	friend struct Z_Construct_UClass_UFMODAsset_Statics; \
public: \
	DECLARE_CLASS(UFMODAsset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FMODStudio"), NO_API) \
	DECLARE_SERIALIZER(UFMODAsset)


#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFMODAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFMODAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFMODAsset); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFMODAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFMODAsset(UFMODAsset&&); \
	NO_API UFMODAsset(const UFMODAsset&); \
public:


#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFMODAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFMODAsset(UFMODAsset&&); \
	NO_API UFMODAsset(const UFMODAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFMODAsset); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFMODAsset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFMODAsset)


#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_PRIVATE_PROPERTY_OFFSET
#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_12_PROLOG
#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_PRIVATE_PROPERTY_OFFSET \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_SPARSE_DATA \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_RPC_WRAPPERS \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_INCLASS \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_PRIVATE_PROPERTY_OFFSET \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_SPARSE_DATA \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_INCLASS_NO_PURE_DECLS \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class FMODAsset."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FMODSTUDIO_API UClass* StaticClass<class UFMODAsset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAsset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
