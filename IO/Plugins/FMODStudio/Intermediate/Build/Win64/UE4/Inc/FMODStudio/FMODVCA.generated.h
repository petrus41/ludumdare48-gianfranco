// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FMODSTUDIO_FMODVCA_generated_h
#error "FMODVCA.generated.h already included, missing '#pragma once' in FMODVCA.h"
#endif
#define FMODSTUDIO_FMODVCA_generated_h

#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_SPARSE_DATA
#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_RPC_WRAPPERS
#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFMODVCA(); \
	friend struct Z_Construct_UClass_UFMODVCA_Statics; \
public: \
	DECLARE_CLASS(UFMODVCA, UFMODAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FMODStudio"), NO_API) \
	DECLARE_SERIALIZER(UFMODVCA)


#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUFMODVCA(); \
	friend struct Z_Construct_UClass_UFMODVCA_Statics; \
public: \
	DECLARE_CLASS(UFMODVCA, UFMODAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FMODStudio"), NO_API) \
	DECLARE_SERIALIZER(UFMODVCA)


#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFMODVCA(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFMODVCA) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFMODVCA); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFMODVCA); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFMODVCA(UFMODVCA&&); \
	NO_API UFMODVCA(const UFMODVCA&); \
public:


#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFMODVCA(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFMODVCA(UFMODVCA&&); \
	NO_API UFMODVCA(const UFMODVCA&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFMODVCA); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFMODVCA); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFMODVCA)


#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_PRIVATE_PROPERTY_OFFSET
#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_11_PROLOG
#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_PRIVATE_PROPERTY_OFFSET \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_SPARSE_DATA \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_RPC_WRAPPERS \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_INCLASS \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_PRIVATE_PROPERTY_OFFSET \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_SPARSE_DATA \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_INCLASS_NO_PURE_DECLS \
	IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class FMODVCA."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FMODSTUDIO_API UClass* StaticClass<class UFMODVCA>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID IO_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODVCA_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
